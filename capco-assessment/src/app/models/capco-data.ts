export class CapcoData {
  name: string;
  phone: string;
  email: string;
  company: string;
  date_entry: any;
  org_num: string;
  address_1: string;
  city: string;
  zip: number;
  geo: string;
  pan: number;
  pin: number;
  id: number;
  status: string;
  fee: string;
  guid: string;
  date_exit: any;
  date_first: any;
  date_recent: any;
  url: string;

  // The parameter ensures that if no object
  // is imported or any data is missing,
  // all values will be instantiated as null
  // and dealt with appropriately
  constructor( capcoDataObject: any = {} ) {
    this.name = capcoDataObject.name || null;
    this.phone = capcoDataObject.phone || null;
    this.email = capcoDataObject.email || null;
    this.company = capcoDataObject.company || null;
    this.date_entry = capcoDataObject.date_entry || null;
    this.org_num = capcoDataObject.org_num || null;
    this.address_1 = capcoDataObject.address_1 || null;
    this.city = capcoDataObject.city || null;
    this.zip = capcoDataObject.zip || null;
    this.geo = capcoDataObject.geo || null;
    this.pan = capcoDataObject.pan || null;
    this.pin = capcoDataObject.pin || null;
    this.id = capcoDataObject.id || null;
    this.status = capcoDataObject.status || null;
    this.fee = capcoDataObject.fee || null;
    this.guid = capcoDataObject.guid || null;
    this.date_exit = capcoDataObject.date_exit || null;
    this.date_first = capcoDataObject.date_first || null;
    this.date_recent = capcoDataObject.date_recent || null;
    this.url = capcoDataObject.url || null;
  }
}
