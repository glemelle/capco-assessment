import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TablePrimaryComponent } from './table-primary/table-primary.component';
import { TableInfiniteComponent } from './table-infinite/table-infinite.component';

// Services
import { LoadDataService } from './services/load-data.service';
import { LoadDataResolver } from './services/load-data-resolver.service';

@NgModule({
   declarations: [
      AppComponent,
      TablePrimaryComponent,
      TableInfiniteComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule
   ],
   providers: [
    LoadDataService,
    LoadDataResolver
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
