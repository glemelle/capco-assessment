/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoadDataResolverService } from './load-data-resolver.service';

describe('Service: LoadDataResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadDataResolverService]
    });
  });

  it('should ...', inject([LoadDataResolverService], (service: LoadDataResolverService) => {
    expect(service).toBeTruthy();
  }));
});
