import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

// models
import { CapcoData } from '../models/capco-data';

@Injectable()
export class LoadDataService {

  constructor(private http: HttpClient) {}

  // Loads Sample data from JSON and stores it in array of CapcoData Objects
  loadCapcoData() {
    const capcoData$ = this.http.get('assets/sample_data.json').pipe (
      map (
        (results: any) => {
          const capcoDataSet = [];
          for (const r of results) {
            capcoDataSet.push(new CapcoData(r));
          }
          return capcoDataSet;
        }
      ));
    return capcoData$;
  }
}
