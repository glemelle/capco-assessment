import { Component, OnInit } from '@angular/core';

// services
import { LoadDataService } from './../services/load-data.service';

// models
import { CapcoData } from './../models/capco-data';

@Component({
  selector: 'app-table-primary',
  templateUrl: './table-primary.component.html',
  styleUrls: ['./table-primary.component.scss']
})
export class TablePrimaryComponent implements OnInit {

  sampleKeys: string[];           // The column Headers
  sampleMasterList: CapcoData[];  // The master list of Data

  displaySampleList: CapcoData[]; // The set displayed for pagination
  paginationCounter: number;      // Multiplier for Pagination Set
  paginationRange: number;        // How many rows to be displayed per page
  paginationStart: number;        // Lower end of Pagination range
  paginationEnd: number;          // Upper end of Pagination range
  maxPagination: number;          // Max value pagination can be set to

  constructor(
    private loadDataService: LoadDataService
  ) {
    this.paginationCounter = 0;
  }

  ngOnInit() {
    this.loadDataService.loadCapcoData().subscribe (
      result => {
        this.sampleKeys = Object.keys(result[0]);
        this.sampleMasterList = result;
        this.paginationRange = this.sampleMasterList.length;
        this.maxPagination = this.sampleMasterList.length;
        this.setPagination(10);
      }
    );
  }

  setPagination(range: number) {
    this.paginationRange = range;
    this.paginationCounter = 0;
    this.paginationStart = 1;
    this.paginationEnd = range;
    this.updateTable();
  }

  updateTable() {
    this.displaySampleList = [];
    for (let s = 0; s < this.paginationRange; s++) {
      this.displaySampleList.push(this.sampleMasterList[s]);
    }
  }

  changePagination(increase: boolean) {
    this.displaySampleList = [];
    increase ? this.paginationCounter++ : this.paginationCounter--;
    this.paginationStart = this.paginationCounter * this.paginationRange + 1;
    this.paginationEnd = this.paginationCounter + 1 * this.paginationRange;
    if (this.paginationEnd > this.maxPagination) {
      this.paginationEnd = this.maxPagination;
    }
    for (let s = this.paginationStart - 1; s < this.paginationEnd; s++) {
      this.displaySampleList.push(this.sampleMasterList[s]);
    }
  }

}
