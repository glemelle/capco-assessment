import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TablePrimaryComponent } from './table-primary/table-primary.component';

const routes: Routes = [
  { path: 'table-primary', component: TablePrimaryComponent },
  { path: '',   redirectTo: '/', pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
